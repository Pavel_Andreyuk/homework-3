import java.util.Comparator;
//если сумма кредита одинакова, то сначала выводится по алфавиту
public class SumOfCreditAndFullNameComparator implements Comparator<Client> {

    @Override
    public int compare(Client o1, Client o2) {
        if (Service.SumOfCredits(o2) - Service.SumOfCredits(o1) != 0) return (int)(Service.SumOfCredits(o2) - Service.SumOfCredits(o1));
        else return o1.getFullName().compareTo(o2.getFullName());
    }
}
