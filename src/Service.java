public class Service {

    static public long SumOfCredits(Client client){
        long sum = 0;
        for (int i=0; i<client.getCredits(); i++){
            sum+=client.getCredit(i).getSum();
        }
        return sum;
    }
}
