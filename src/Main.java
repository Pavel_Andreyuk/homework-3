import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {

        Client first = new Client("Ivanov Ivan", 20);
        Client second = new Client("Antonov Anton", 32);
        Client third = new Client("Bezrukov Petr", 45);
        Client fourth = new Client("Drozdov Nikolay", 60);
        Client fifth = new Client("Zokin Pavel", 50);
        Client sixth = new Client("Alexeev Alexey", 33);
        Client seventh = new Client("Callaghan Tom", 28);

        first.setCredit(100, 10, 100);
        first.setCredit(500, 10, 50);
        second.setCredit(300, 10, 30);
        third.setCredit(600,10,60);
        fifth.setCredit(600, 10,60);
        sixth.setCredit(600, 10,60);
        seventh.setCredit(600,10,60);

        System.out.println(Service.SumOfCredits(first));

        ArrayList<Client> list = new ArrayList<>();

        list.add(first);
        list.add(second);
        list.add(third);
        list.add(fourth);
        list.add(fifth);
        list.add(sixth);
        list.add(seventh);

        System.out.println("Unsorted list:" + "\n" + list);

        Collections.sort(list);

        System.out.println();

        System.out.println("Sorted by full name:" + "\n" + list);

        Comparator sumReverse = new SumOfCreditComparatorReverse();
        Comparator sum = new SumOfCreditComparator();
        Comparator sumAndFullName = new SumOfCreditAndFullNameComparator();

        System.out.println("Sorted by credit. The min is first: ");
        Collections.sort(list, sumReverse);

        System.out.println(list);
        System.out.println("Sorted by credit. The max is first: ");
        Collections.sort(list, sum);
        System.out.println(list);
        System.out.println();

        //сортировка по имени и по сумме кредита, если сумма кредита одинакова у n клиентов, то все клиента с одинаковой суммой сначала сортируются по алфавиту
        Collections.sort(list, sumAndFullName);
        System.out.println("Sorted by credit: ");
        System.out.println(list);
    }
}