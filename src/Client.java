import java.util.ArrayList;

public class Client implements Comparable<Client>{

    private String fullName;;
    private int age;
    public ArrayList<Credit> credit = new ArrayList<>();

    public Client(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }

    public int compareTo(Client o){
        return this.getFullName().compareTo(o.getFullName());
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String name) {
        this.fullName = name;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCredit(long sum, int term, int payment){
        credit.add(new Credit(sum, term, payment));
    }

    @Override
    public String toString() {
        return "Client{" +
                "fullName='" + fullName + '\'' +
                ", age=" + age +
                ", credit=" + credit +
                '}' +  "\n";
    }

    public Credit getCredit(int i){
        return credit.get(i);
    }

    public int getCredits(){
        return credit.size();
    }
}
