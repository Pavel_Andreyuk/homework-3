public class Credit {

    private long sum;
    private int term;
    private int payment;

    public Credit(long sum, int term, int payment) {
        this.sum = sum;
        this.term = term;
        this.payment = payment;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "Credit{" +
                "sum=" + sum +
                ", term=" + term +
                ", payment=" + payment +
                '}';
    }


}
