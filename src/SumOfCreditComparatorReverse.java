import java.util.Comparator;
//компаратор сравнения по сумме кредита от меньшего к большему
public class SumOfCreditComparatorReverse implements Comparator<Client> {

    @Override
    public int compare(Client o1, Client o2) {
        return (int)(Service.SumOfCredits(o1) - Service.SumOfCredits(o2));
    }
}