import java.util.Comparator;
//компаратор сравнения по сумме кредита от большего к меньшему
public class SumOfCreditComparator implements Comparator<Client> {

    @Override
    public int compare(Client o1, Client o2) {
        return (int)(Service.SumOfCredits(o2) - Service.SumOfCredits(o1));
    }
}